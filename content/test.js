const wrapper = document.getElementById("grid-container");
let columns = 0;
let rows = 0;

const handleOnClick = index => {
	for (let i = index - 1; i <= index + 1; i++) {
		for (let j = index - 1; j <= index + 1; j++) {
			let tile = wrapper.getElementsByTagName("tile"+(index+i+j));
			// tile.style
		}
	}
}

const createTile = index => {
	const tile = document.createElement("div");
	tile.classList.add("tile");
	tile.id = ("tile"+index)
	tile.onclick = e => handleOnClick(index);
	return tile;
}

const createTiles = quantity => {
	Array.from(Array(quantity)).map((tile, index) => {
	wrapper.appendChild(createTile(index));
	});
}

const createGrid = () => {
	wrapper.innerHTML = "";
	columns = Math.floor(document.body.clientWidth / 50 );
	rows = Math.floor(document.body.clientHeight / 50 );
	wrapper.style.setProperty("--columns", columns);
	wrapper.style.setProperty("--rows", rows);
	createTiles(columns * rows);
}

createGrid();

window.onresize = () => createGrid();
