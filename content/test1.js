const canvas = document.getElementById("canvas");
let columns = 0;
let rows = 0;

const handleOnClick = (index) => {
	for (let i = 0; i < 1; i++) {
		for (let j = 0; j < 1; j++) {
            console.log(index);
		}
	}
}

const testFunc = (r, g, b) => {
    return `rgb(${r}, ${g}, ${b})`
}

const createGrid = () => {
    columns = 1080;
    rows = 1920;
    canvas.style.setProperty("--columns", columns);
	canvas.style.setProperty("--rows", rows);
    console.log(columns, rows);
	let canvastx = canvas.getContext("2d");
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            let color = testFunc(255, 0, 255);
            canvastx.fillStyle = color;
            canvastx.fillRect(i, j, 1, 1);
        }
    
    }
    // let r = 255;
    // let g = 0;
    // let b = 255;
	// ctx.fillStyle = testFunc(r, g, b);
    // ctx.fillRect(20, 20, 75, 50);
    // ctx.globalAlpha = 0.25;
    // ctx.fillStyle = 'rgb(0, 0, 255)';
    // ctx.fillRect(50, 50, 75, 50);
    // ctx.fillStyle = 'rgb(0, 255, 0)';
    // ctx.fillRect(80, 80, 75, 50);
}

createGrid();

window.onresize = () => createGrid();
