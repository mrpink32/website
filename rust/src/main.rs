use std::net::TcpStream;
use std::path::Path;
use std::sync::Arc;
use std::io::{BufReader, BufRead};

use logging::*;
use webserver::*;

const CONFIG_PATH: &str = "/mnt/data/repositories/website/config.json";
// const LOG_PATH: &str = "/mnt/data/repositories/website/log.txt";
const CONTENT_PATH: &str = "/mnt/data/repositories/website/content";

 
fn handle_connection(mut stream: TcpStream, logger: Arc<Logger>, content_path: String) {
    let buf_reader: BufReader<&mut TcpStream> = BufReader::new(&mut stream);
    let http_request: Vec<String> = buf_reader
        .lines()
        .filter_map(|result| result.ok())
        .take_while(|line| !line.is_empty())
        .collect();
    println!("Request: {:#?}", http_request);
    logger.log(format!("Request: {:#?}", http_request).as_str());

    if http_request.len() < 1 {
        return;
    }

    let mut http_command = Request::new(&http_request[0]);

    println!("Commands: {:#?}", http_command);

    if http_command.get_path() == "/" {
        http_command.set_path(String::from("/index.xhtml"));
    }

    let mut filename: String = format!("{}{}", content_path, http_command.get_path());
    println!("Filename: {}", filename);
    let mut filepath: &Path = Path::new(filename.as_str());

    let mut needs_body = true;
    let mut status_line = "HTTP/1.1 404 NOT FOUND";
    // let mut status = 404;
    // match Method::from(http_command[0]) {
    match http_command.get_method() {
        Method::Get => match Path::try_exists(filepath) {
            Ok(exists) => {
                status_line = if exists {
                    let status_line = "HTTP/1.1 200 OK";
                    // status = 200;
                    logger.log(
                        format!(
                            "Item found: {}, Status: {}",
                            filepath.to_str().unwrap(),
                            status_line
                        )
                        .as_str(),
                    );
                    status_line
                } else {
                    logger.log_error(
                        format!(
                            "Item not found: {}, Status: {}",
                            filepath.to_str().unwrap(),
                            status_line
                        )
                        .as_str(),
                    );
                    filename = format!("{}/error.html", content_path);
                    filepath = Path::new(filename.as_str());
                    status_line
                };
            }
            Err(error) => {
                logger.log_error(error.to_string().as_str());
                eprintln!("{}", error);
                filename = format!("{}/error.html", content_path);
                filepath = Path::new(filename.as_str());
            }
        },
        Method::Head => match Path::try_exists(filepath) {
            Ok(exists) => {
                status_line = if exists {
                    let status_line = "HTTP/1.1 200 OK";
                    logger.log(
                        format!(
                            "Item found: {}, Status: {}",
                            filepath.to_str().unwrap(),
                            status_line
                        )
                        .as_str(),
                    );
                    needs_body = false;
                    status_line
                } else {
                    logger.log_error(
                        format!(
                            "Item not found: {}, Status: {}",
                            filepath.to_str().unwrap(),
                            status_line
                        )
                        .as_str(),
                    );
                    filename = format!("{}/error.html", content_path);
                    filepath = Path::new(filename.as_str());
                    status_line
                };
            }
            Err(error) => {
                logger.log_error(error.to_string().as_str());
                filename = format!("{}/error.html", content_path);
                filepath = Path::new(filename.as_str());
            }
        },
        Method::Post => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Put => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Delete => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Connect => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Options => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Trace => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::Patch => {
            status_line = "HTTP/1.1 405 Method Not Allowed";
        }
        Method::BadRequest => {
            status_line = "HTTP/1.1 400 Bad Request";
            logger.log(
                format!(
                    "{} has no implementation!",
                    http_command.get_method().to_string()
                )
                .as_str(),
            );
            eprintln!(
                "{} has no implementation!",
                http_command.get_method().to_string()
            );
        }
    };
    // let response = Response::builder()
    //     .status(status)
    //     .content(filepath)
    //     .build();
    // response.write(stream);
    write_response(stream, filepath, status_line, needs_body);
}

fn main() {
    let server: Webserver = Webserver::new(CONFIG_PATH, CONTENT_PATH);
    let listener = server.start(None, None);

    let pool: ThreadPool = ThreadPool::new(5);
    for stream in listener.incoming() {
        let logger_copy = Arc::clone(&server.logger);
        let content_path = server.content_path.clone();
        match stream {
            Ok(stream) => {
                pool.execute(|| {
                    handle_connection(stream, logger_copy, content_path);
                });
            }
            Err(error) => {
                server.logger.log_error(format!("{error}").as_str());
                // panic!("Error: {}", e);
            }
        }
    }
    println!("Shutting down...");
}


