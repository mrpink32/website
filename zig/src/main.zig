const std = @import("std");
const net = std.net;
const builtin = @import("builtin");
const target = builtin.target;
const allocator = std.heap.page_allocator;
const c = @cImport({
    @cInclude("pthreads");
});

const testing = std.testing;

const PORT: u16 = 80;
const IP: [4]u8 = .{ 127, 0, 0, 1 };

const BUFFER_SIZE: u32 = 1024;

pub fn openFile() !void {}

pub fn getFileSize(file: std.fs.File) !u64 {
    const file_stat = try file.stat();
    const file_size = file_stat.size;
    // _ = try stdout.print("file size: {}\n", .{file_size});
    std.debug.print("file size: {}\n", .{file_size});
    return file_size;
}

pub fn handleConnection(client: net.StreamServer.Connection, dir: std.fs.Dir) void {
    var message: [BUFFER_SIZE]u8 = undefined;
    // Deprecated: use streamUntilDelimiter with FixedBufferStream’s writer instead.
    _ = client.stream.reader().readUntilDelimiter(&message, '\r') catch |err| {
        // _ = stdout.print("Failed to read from client stream! Error: {}", .{err});
        std.debug.print("Failed to read from client stream! Error: {}", .{err});
        return;
    };
    
    const thread_id = std.Thread.getCurrentId();
    std.debug.print("Thread id: {}\n", .{thread_id});

    if (message.len > 0) {
        // _ = try stdout.print("{s}\n", .{message});
        std.debug.print("{s}\n", .{message});

        var method_field = std.mem.splitAny(u8, &message, " ");
        var list = std.ArrayList([]const u8).init(allocator);
        defer list.deinit();
        while (method_field.peek() != null) {
            const placeholder = method_field.next().?;
            // _ = try stdout.print("test: {s}\n", .{placeholder});
            std.debug.print("item: {s}\n", .{placeholder});
            list.append(placeholder) catch |err| {
                std.debug.print("Failed to append to list! Error: {}", .{err});
                return;
            };
        }
        if (list.items.len < 2) {
            std.debug.print("Request doesn't contain a destination!", .{});
        }
        var destination = list.items[1];

        if (std.mem.eql(u8, destination, "/")) {
            destination = "/index.xhtml";
        }

        // _ = try stdout.print("destination: {s}\n", .{destination[1..]});
        std.debug.print("destination: {s}\n", .{destination[1..]});

        const file: std.fs.File = dir.openFile(destination[1..], std.fs.File.OpenFlags{}) catch |err| {
            // _ = try stdout.print("Failed to open file! Error: {}\n", .{err});
            std.debug.print("Failed to open file! Error: {}\n", .{err});
            return;
        };
        defer file.close();

        const file_size = getFileSize(file) catch |err| {
            // _ = try stdout.print("Failed to get size of file! Error: {}", .{err});
            std.debug.print("Failed to get size of file! Error: {}", .{err});
            return;
        };

        const file_content = allocator.alloc(u8, file_size) catch |err| {
            std.debug.print("Error: {}", .{err});
            return;
        };
        defer allocator.free(file_content);

        const bytes_read = file.reader().read(file_content) catch |err| {
            // _ = try stdout.print("Failed to read file! Error: {}", .{err});
            std.debug.print("Failed to read file! Error: {}", .{err});
            return;
        };
        // _ = try stdout.print("Read {d} bytes from file!\n", .{bytes_read});
        std.debug.print("Read {d} bytes from file!\n", .{bytes_read});

        // send HTTP respons
        writeResponse(client.stream, file_size, file_content) catch |err| {
            // _ = try stdout.print("Failed to write response to client socket! Error: {}", .{err});
            std.debug.print("Failed to write response to client socket! Error: {}", .{err});
            return;
        };
    }
    // _ = try stdout.print("closing connection!\n", .{});
    std.debug.print("closing connection!\n", .{});
    client.stream.close();
}

pub fn writeResponse(client_stream: std.net.Stream, content_length: u64, content: []const u8) !void {
    try std.fmt.format(client_stream.writer(), "HTTP/1.1 200 OK\r\nContent-Length: {}\r\n\r\n{s}", .{ content_length, content });
}

pub fn main() !void {
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    // const stdout = bw.writer();

    var dir: std.fs.Dir = std.fs.openDirAbsolute("/mnt/data/repositories/website/content", std.fs.Dir.OpenDirOptions{}) catch |err| {
        // _ = try stdout.print("Failed to open file! Error: {}\n", .{err});
        std.debug.print("Failed to open file! Error: {}\n", .{err});
        return err;
    };
    defer dir.close();

    const temp = net.StreamServer.Options{
        .kernel_backlog = 128,
        .reuse_port = true,
        .reuse_address = true,
    };

    var listener = net.StreamServer.init(temp);
    defer listener.deinit();

    const address = net.Address.initIp4(IP, PORT);
    listener.listen(address) catch |err| {
        // _ = try stdout.print("Failed to initialize listener! Error: {}", .{err});
        std.debug.print("Failed to initialize listener! Error: {}", .{err});
        return err;
    };

    var thread_pool = std.Thread.Pool {
        .allocator = allocator,
        .threads = &[_]std.Thread{},
    };
    const options = std.Thread.Pool.Options {
        .allocator = allocator,
        .n_jobs = 5,
    };
    try thread_pool.init(options);
    defer thread_pool.deinit();

    while (true) {
        std.debug.print("waiting for client connection...\n", .{});
        const client = listener.accept() catch |err| {
            // _ = try stdout.print("Failed to accept client! Error:{}", .{err});
            std.debug.print("Failed to accept client! Error:{}", .{err});
            continue;
        };
        //  _ = try stdout.print("connection from: {}\n", .{client.address});
        std.debug.print("incomming connection from: {}\n", .{client.address});

        try thread_pool.spawn(handleConnection, .{client, dir});
        // handleConnection(client, dir);
    }
    // _ = try stdout.print("shutting down!\n", .{});
    std.debug.print("shutting down!", .{});

    try bw.flush();
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
